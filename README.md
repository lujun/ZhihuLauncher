# Android中的视差滚动效果
### 类似知乎引导页、Yahoo Weather App

## 原文参考
##### [http://developer.android.com/intl/zh-cn/training/animation/screen-slide.html#pagetransformer](http://developer.android.com/intl/zh-cn/training/animation/screen-slide.html#pagetransformer)
##### [https://github.com/xgc1986/ParallaxPagerTransformer](https://github.com/xgc1986/ParallaxPagerTransformer)
##### [http://andraskindler.com/blog/2013/create-viewpager-transitions-a-pagertransformer-example/](http://andraskindler.com/blog/2013/create-viewpager-transitions-a-pagertransformer-example/)
##### [https://medium.com/@BashaChris/the-android-viewpager-has-become-a-fairly-popular-component-among-android-apps-its-simple-6bca403b16d4](https://medium.com/@BashaChris/the-android-viewpager-has-become-a-fairly-popular-component-among-android-apps-its-simple-6bca403b16d4)

##### 有任何问题，[email me](mailto:lujunat1993@gmail.com).

## License
Copyright (c) 2015 [lujun](http://lujun.co)

Licensed under the [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.html)