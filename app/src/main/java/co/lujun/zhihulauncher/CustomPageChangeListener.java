package co.lujun.zhihulauncher;

import android.animation.ArgbEvaluator;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

/**
 * Created by lujun on 2015/9/26.
 */
public class CustomPageChangeListener implements ViewPager.OnPageChangeListener{

    private ArgbEvaluator mColorEvaluator;
    private int mPageWidth, mTotalScrollWidth;
    private int mStartColor, mEndColor;
    private ViewPager mViewPager;
    private PagerAdapter mPageAdapter;

    public CustomPageChangeListener(Context context, int screenWidth,ViewPager viewPager, PagerAdapter adapter, int startColor, int endColor){
        this.mViewPager = viewPager;
        this.mPageAdapter = adapter;
        mColorEvaluator = new ArgbEvaluator();
        mPageWidth = screenWidth;

        mStartColor = context.getResources().getColor(startColor);
        mEndColor = context.getResources().getColor(endColor);
    }

    @Override public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        mTotalScrollWidth = mPageWidth * mPageAdapter.getCount();
        float ratio = (mPageWidth * position + positionOffsetPixels) / (float) mTotalScrollWidth;
        Integer color = (Integer) mColorEvaluator.evaluate(ratio, mStartColor, mEndColor);
        mViewPager.setBackgroundColor(color);
    }

    @Override public void onPageScrollStateChanged(int state) {

    }

    @Override public void onPageSelected(int position) {

    }
}