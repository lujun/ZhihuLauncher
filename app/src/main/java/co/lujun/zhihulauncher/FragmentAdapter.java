package co.lujun.zhihulauncher;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lujun on 2015/9/26.
 */
public class FragmentAdapter extends FragmentStatePagerAdapter {

    private List<Fragment> mFragmentList = new ArrayList<Fragment>();
    private ViewPager mViewPager;

    public FragmentAdapter(FragmentManager manager) {
        super(manager);
    }

    public void addItem(Fragment fragment) {
        mFragmentList.add(fragment);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // 阻止重复销毁和加载, need // bleow code
        super.destroyItem(container, position, object);
    }

    /**
     * add fragment
     * @param fragment
     */
    public void add(BaseFragment fragment){
        mFragmentList.add(fragment);
        notifyDataSetChanged();
        mViewPager.setCurrentItem(getCount() - 1, true);
    }

    /**
     * remove the fragment by fragment
     * @param fragment
     */
    public void remove(BaseFragment fragment){
        if (fragment != null){
            mFragmentList.remove(fragment);
            notifyDataSetChanged();
            int pos = mViewPager.getCurrentItem();
            mViewPager.setAdapter(this);
            if (pos >= getCount()){
                pos = getCount() - 1;
            }
            mViewPager.setCurrentItem(pos, true);
        }
    }

    /**
     * remove the fragment by fragment position
     * @param i
     */
    public void remove(int i){
        remove((BaseFragment)getItem(i));
    }

    /**
     * set viewpager
     * @param pager
     */
    public void setViewPager(ViewPager pager){
        this.mViewPager = pager;
    }
}