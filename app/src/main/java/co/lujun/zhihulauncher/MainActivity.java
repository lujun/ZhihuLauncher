package co.lujun.zhihulauncher;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import co.lujun.zhihulauncher.zhihu.ZhihuActivity;

public class MainActivity extends AppCompatActivity {

    private ViewPager mViewPager;
    private FragmentAdapter mPageAdapter;
    private ParallaxTransformer mParallaxTransformer;
    private CustomPageChangeListener mPageChangeListener;

    private final static String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // init
        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mPageAdapter = new FragmentAdapter(getSupportFragmentManager());
        mParallaxTransformer = new ParallaxTransformer(R.id.imageview);
        mPageChangeListener = new CustomPageChangeListener(this,
                getWindowManager().getDefaultDisplay().getWidth(),
                mViewPager,
                mPageAdapter,
                R.color.page_start_color,
                R.color.page_end_color);

        mViewPager.setPageTransformer(false, mParallaxTransformer);
        mViewPager.setOnPageChangeListener(mPageChangeListener);
        mPageAdapter.setViewPager(mViewPager);
        mParallaxTransformer.setBorder(5);

        //
        Bundle bNina = new Bundle();
        bNina.putInt("image", R.mipmap.bg_kero);
        BaseFragment pfNina = new BaseFragment();
        pfNina.setArguments(bNina);

        Bundle bNiju = new Bundle();
        bNiju.putInt("image", R.mipmap.bg_niju);
        BaseFragment pfNiju = new BaseFragment();
        pfNiju.setArguments(bNiju);

        Bundle bYuki = new Bundle();
        bYuki.putInt("image", R.mipmap.bg_yuki);
        BaseFragment pfYuki = new BaseFragment();
        pfYuki.setArguments(bYuki);

        mPageAdapter.add(pfNina);
        mPageAdapter.add(pfNiju);
        mPageAdapter.add(pfYuki);
        mViewPager.setAdapter(mPageAdapter);

        Button btnAdd = (Button) findViewById(R.id.btn_add_page);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bYuki = new Bundle();
                bYuki.putInt("image", R.mipmap.bg_yuki);
                BaseFragment pfYuki = new BaseFragment();
                pfYuki.setArguments(bYuki);
                mPageAdapter.add(pfYuki);
            }
        });
        Button btnRm = (Button) findViewById(R.id.btn_rm_page);
        btnRm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPageAdapter.remove((BaseFragment)mPageAdapter.getItem(mViewPager.getCurrentItem()));
            }
        });
        Button btnZhihu = (Button) findViewById(R.id.btn_zhihu_welcome);
        btnZhihu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, ZhihuActivity.class));
            }
        });
    }
}
