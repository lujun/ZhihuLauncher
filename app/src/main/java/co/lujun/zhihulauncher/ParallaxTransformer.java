package co.lujun.zhihulauncher;

import android.os.Build;
import android.support.v4.view.ViewPager;
import android.view.View;

/**
 * Created by lujun on 2015/9/26.
 */
public class ParallaxTransformer implements ViewPager.PageTransformer{

    int id;
    float speed = 0.2f;
    float border = 0;

    public ParallaxTransformer(int id) {
        this.id = id;
    }

    public void setSpeed(float speed){
        this.speed = speed;
    }

    public void setBorder(float border){
        this.border = border;
    }

    @Override public void transformPage(View page, float position) {
        View parallaxView = page.findViewById(id);

        if (parallaxView != null && Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB) {
            if (position > -1 && position < 1) {
                float width = parallaxView.getWidth();
                parallaxView.setTranslationX(-(position * width * speed));
                float sc = ((float)page.getWidth() - border)/ page.getWidth();
                if (position == 0) {
                    page.setScaleX(1);
//                    page.setScaleY(1);
                } else {
                    page.setScaleX(sc);
//                    page.setScaleY(sc);
                }
            }
        }
    }
}