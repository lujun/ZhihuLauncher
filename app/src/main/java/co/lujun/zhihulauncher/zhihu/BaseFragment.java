package co.lujun.zhihulauncher.zhihu;

import android.support.v4.app.Fragment;

/**
 * Created by lujun on 2015/9/25.
 */
public abstract class BaseFragment extends Fragment {

    public abstract int[] getChildViewIds();
    public abstract int getRootId();
}
