package co.lujun.zhihulauncher.zhihu;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import co.lujun.zhihulauncher.R;

/**
 * Created by lujun on 2015/9/25.
 */
public class OnePageFragment extends BaseFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.fragment_zh_1, container, false);
    }

    @Override
    public int getRootId() {
        return R.id.ll_fragment_zh_1;
    }

    @Override
    public int[] getChildViewIds() {
        return new int[]{
                R.id.tv_fragment_zh_1,
                R.id.v_fragment_11,
                R.id.v_fragment_12,
                R.id.v_fragment_13,
                R.id.v_fragment_14
        };
    }
}
