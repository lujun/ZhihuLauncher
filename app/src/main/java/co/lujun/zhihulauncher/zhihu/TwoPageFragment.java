package co.lujun.zhihulauncher.zhihu;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import co.lujun.zhihulauncher.R;

/**
 * Created by lujun on 2015/9/25.
 */
public class TwoPageFragment extends BaseFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.fragment_zh_2, container, false);
    }

    @Override
    public int getRootId() {
        return R.id.ll_fragment_zh_2;
    }

    @Override
    public int[] getChildViewIds() {
        return new int[]{
                R.id.tv_fragment_zh_2,
                R.id.tv_fragment_zh_21,
                R.id.v_fragment_21,
                R.id.v_fragment_22,
                R.id.v_fragment_23,
                R.id.v_fragment_24
        };
    }
}
