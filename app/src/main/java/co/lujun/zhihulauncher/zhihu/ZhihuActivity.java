package co.lujun.zhihulauncher.zhihu;

import android.animation.ArgbEvaluator;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import co.lujun.zhihulauncher.R;

public class ZhihuActivity extends AppCompatActivity {

    private ViewPager mViewPager;
    private FragmentAdapter mPageAdapter;

    SparseArray<int[]> mLayoutViewIdsMap = new SparseArray<int[]>();

    final float PARALLAX_COEFFICIENT = 1.2f;// 1.2f
    final float DISTANCE_COEFFICIENT = 0.5f;// 0.5f

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // init
        mViewPager = (ViewPager) findViewById(R.id.viewpager);

        mPageAdapter = new FragmentAdapter(getSupportFragmentManager());

        addPage(new OnePageFragment());
        addPage(new TwoPageFragment());
        addPage(new ThreePageFragment());

        mViewPager.setPageTransformer(false, new ParallaxTransformer(PARALLAX_COEFFICIENT, DISTANCE_COEFFICIENT, R.id.imageview));
        mViewPager.setOnPageChangeListener(new CustomPagerChangeListener());
        mViewPager.setAdapter(mPageAdapter);
    }

    /**
     * add page 4 FragmentAdapter
     * @param fragment
     */
    private void addPage(BaseFragment fragment){
        mPageAdapter.addItem(fragment);
        mLayoutViewIdsMap.put(fragment.getRootId(), fragment.getChildViewIds());
    }

    /**
     * ParallaxTransformer
     */
    class ParallaxTransformer implements ViewPager.PageTransformer{

        float parallaxCoefficient;
        float distanceCoefficient;
        int id;

        public ParallaxTransformer(float parallaxCoefficient, float distanceCoefficient, int id) {
            this.parallaxCoefficient = parallaxCoefficient;
            this.distanceCoefficient = distanceCoefficient;
            this.id = id;
        }

        @Override public void transformPage(View page, float position) {
            float scrollXOffset = page.getWidth() * parallaxCoefficient;

            ViewGroup pagerViewWrapper = (ViewGroup) page;

            int[] layer = mLayoutViewIdsMap.get(pagerViewWrapper.getId());
            if (layer != null){
                for (int id : layer) {
                    View view = page.findViewById(id);
                    if (view != null){
                        view.setTranslationX(scrollXOffset * position);
                    }
                    scrollXOffset *= distanceCoefficient;
                }
            }
        }
    }

    /**
     * CustomPagerChangeListener
     */
    class CustomPagerChangeListener implements ViewPager.OnPageChangeListener{

        ArgbEvaluator mColorEvaluator;
        int mPageWidth, mTotalScrollWidth;
        int mStartColor, mEndColor;

        public CustomPagerChangeListener(){
            mColorEvaluator = new ArgbEvaluator();
            mPageWidth = getWindowManager().getDefaultDisplay().getWidth();
            mTotalScrollWidth = mPageWidth * mPageAdapter.getCount();

            mStartColor = getResources().getColor(R.color.page_start_color);
            mEndColor = getResources().getColor(R.color.page_end_color);
        }

        @Override public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            float ratio = (mPageWidth * position + positionOffsetPixels) / (float) mTotalScrollWidth;
            Integer color = (Integer) mColorEvaluator.evaluate(ratio, mStartColor, mEndColor);
            mViewPager.setBackgroundColor(color);
        }

        @Override public void onPageScrollStateChanged(int state) {

        }

        @Override public void onPageSelected(int position) {

        }
    }

    /**
     * FragmentAdapter
     */
    class FragmentAdapter extends FragmentStatePagerAdapter {

        private List<Fragment> mFragmentList = new ArrayList<Fragment>();

        public FragmentAdapter(FragmentManager manager){
            super(manager);
        }

        public void addItem(Fragment fragment){
            mFragmentList.add(fragment);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }
    }
}
